#pragma once
#include <knx.h>

// Parameter with single occurance
#define BIN_StartupDelay               0      // int32_t
#define BIN_StartupDelaySelection      4      // char*, 2 Byte
#define BIN_Heartbeat                  6      // int32_t
#define BIN_LEDdisplay                10      // 1 Bit, Bit 7
#define     BIN_LEDdisplayMask 0x80
#define     BIN_LEDdisplayShift 7
#define BIN_LEDdisplayKOActivation    11      // 1 Bit, Bit 7
#define     BIN_LEDdisplayKOActivationMask 0x80
#define     BIN_LEDdisplayKOActivationShift 7
#define BIN_LEDdisplayDirection       12      // 1 Bit, Bit 7
#define     BIN_LEDdisplayDirectionMask 0x80
#define     BIN_LEDdisplayDirectionShift 7
#define BIN_InputReadTime             13      // 8 Bits, Bit 7-0
#define BIN_InactiveCHLED             14      // 1 Bit, Bit 7
#define     BIN_InactiveCHLEDMask 0x80
#define     BIN_InactiveCHLEDShift 7

// Parameter per channel
#define BIN_ParamBlockOffset 15
#define BIN_ParamBlockSize 60
#define BIN_CHBuswiederkehrAbfrage    50      // 1 Bit, Bit 7
#define     BIN_CHBuswiederkehrAbfrageMask 0x80
#define     BIN_CHBuswiederkehrAbfrageShift 7
#define BIN_CHSendenZyklisch          51      // int32_t
#define BIN_CHOutputValue             55      // 1 Bit, Bit 7
#define     BIN_CHOutputValueMask 0x80
#define     BIN_CHOutputValueShift 7
#define BIN_CHFallWertSenden          56      // 1 Bit, Bit 7
#define     BIN_CHFallWertSendenMask 0x80
#define     BIN_CHFallWertSendenShift 7
#define BIN_CHSteigWertSenden         57      // 1 Bit, Bit 7
#define     BIN_CHSteigWertSendenMask 0x80
#define     BIN_CHSteigWertSendenShift 7
#define BIN_CHSperren                 58      // 1 Bit, Bit 7
#define     BIN_CHSperrenMask 0x80
#define     BIN_CHSperrenShift 7
#define BIN_CHFunktion                59      // 1 Bit, Bit 7
#define     BIN_CHFunktionMask 0x80
#define     BIN_CHFunktionShift 7

// Communication objects per channel (multiple occurance)
#define BIN_KoOffset 50
#define BIN_KoBlockSize 2
#define BIN_KoGO_STATE_ 0
#define BIN_KoGO_SPERR_ 1

// Communication objects with single occurance
#define BIN_KoHeartbeat 1
#define BIN_KoStatusLedKO 2

