#pragma once


//Variant handling
//#define V1
#define V2


#define SERIAL_PORT SerialUSB   // Serial port for Arduino Zero

// MUst be updated to the new Project HW !!!


#define MAX_NUMBER_OF_Channels 16


#ifdef V1
#define PROG_LED_PIN A2
#define PROG_LED_PIN_ACTIVE_ON HIGH
#define PROG_BUTTON_PIN A1
#define PROG_BUTTON_PIN_INTERRUPT_ON FALLING
#define SAVE_INTERRUPT_PIN 38 
#define LED_YELLOW_PIN 25
// Set i2c address
#define  i2cAddr_IO   0x20
#define  i2cAddr_VCC  0x23
#define  i2cAddr_LED  0x2
#endif

#ifdef V2
#define PROG_LED_PIN 9
#define PROG_LED_PIN_ACTIVE_ON HIGH
#define PROG_BUTTON_PIN 8
#define PROG_BUTTON_PIN_INTERRUPT_ON FALLING
#define SAVE_INTERRUPT_PIN 38 
#define LED_YELLOW_PIN 22
// Set i2c address
#define  i2cAddr_IO   0x20
#define  i2cAddr_VCC  0x3B
#define  i2cAddr_LED  0x24
#endif





